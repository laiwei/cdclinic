options:
  parameters:
    author: ''
    catch_exceptions: 'True'
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: University of Melbourne
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: cdc_packet_hw_loopback
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: CDC Packet HW Loopback
    window_size: (1000,1000)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: alg
  id: variable_adaptive_algorithm
  parameters:
    comment: ''
    cons: bpsk
    delta: '10.0'
    ffactor: '0.99'
    modulus: '4'
    step_size: '.0001'
    type: cma
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [560, 1220.0]
    rotation: 0
    state: enabled
- name: bps_payload
  id: variable_qtgui_chooser
  parameters:
    comment: ''
    gui_hint: 1, 0, 1, 1
    label: Payload Constellation
    label0: BPSK
    label1: QPSK
    label2: 8PSK
    label3: 16QAM
    label4: ''
    labels: '[]'
    num_opts: '3'
    option0: '1'
    option1: '2'
    option2: '3'
    option3: '4'
    option4: '4'
    options: '[0, 1, 2]'
    orient: Qt.QVBoxLayout
    type: int
    value: '2'
    widget: combo_box
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [808, 12.0]
    rotation: 0
    state: enabled
- name: bpsk
  id: variable_constellation
  parameters:
    comment: ''
    const_points: '[-1-1j, -1+1j, 1+1j, 1-1j]'
    dims: '1'
    normalization: digital.constellation.AMPLITUDE_NORMALIZATION
    precision: '8'
    rot_sym: '4'
    soft_dec_lut: None
    sym_map: '[0, 1, 3, 2]'
    type: qpsk
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [144, 636.0]
    rotation: 0
    state: enabled
- name: default_freq
  id: variable
  parameters:
    comment: ''
    value: 920e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [312, 12.0]
    rotation: 0
    state: enabled
- name: default_tx_gain
  id: variable
  parameters:
    comment: ''
    value: '30'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [296, 76.0]
    rotation: 0
    state: enabled
- name: excess_bw
  id: variable
  parameters:
    comment: ''
    value: '0.35'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [192, 76.0]
    rotation: 0
    state: enabled
- name: phase_bw
  id: variable
  parameters:
    comment: ''
    value: '0.0628'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [384, 1220.0]
    rotation: 0
    state: enabled
- name: pld_data
  id: variable
  parameters:
    comment: ''
    value: np.random.randint(0, 256, 20, dtype=np.uint8)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [72, 244.0]
    rotation: 0
    state: disabled
- name: rf_freq
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: 0, 0, 1, 1
    label: RF Frequency (Hz)
    min_len: '200'
    orient: QtCore.Qt.Horizontal
    rangeType: float
    start: 70e6
    step: 1e3
    stop: 6e9
    value: default_freq
    widget: counter
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [416, 12.0]
    rotation: 0
    state: enabled
- name: rx_gain
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: 1, 1, 1, 1
    label: Rx Gain (dB)
    min_len: '200'
    orient: QtCore.Qt.Horizontal
    rangeType: float
    start: '-10'
    step: '1'
    stop: '60'
    value: '30'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [688, 12.0]
    rotation: 0
    state: enabled
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: symb_rate * sps
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [440, 220.0]
    rotation: 0
    state: disabled
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: '550000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [312, 636.0]
    rotation: 0
    state: enabled
- name: sps
  id: variable
  parameters:
    comment: ''
    value: '2'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [360, 212.0]
    rotation: 0
    state: disabled
- name: sps
  id: variable
  parameters:
    comment: ''
    value: '4'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [480, 1220.0]
    rotation: 0
    state: enabled
- name: symb_rate
  id: variable
  parameters:
    comment: ''
    value: '275000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [256, 212.0]
    rotation: 0
    state: disabled
- name: thresh
  id: variable
  parameters:
    comment: ''
    value: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [712, 1220.0]
    rotation: 0
    state: enabled
- name: tx_gain
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: 0, 1, 1, 1
    label: Tx Gain (dB)
    min_len: '200'
    orient: QtCore.Qt.Horizontal
    rangeType: float
    start: '-10'
    step: '1'
    stop: '50'
    value: default_tx_gain
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 12.0]
    rotation: 0
    state: enabled
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: '0.5'
    comment: ''
    freq: '1000'
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [912, 676.0]
    rotation: 0
    state: disabled
- name: bladeRF_sink_0
  id: bladeRF_sink
  parameters:
    affinity: ''
    alias: ''
    bias_tee0: 'False'
    bias_tee1: 'False'
    bw: '0'
    comment: ''
    dac: '10000'
    dc_calibration: LPF_TUNING
    device_id: '0'
    fpga_image: ''
    fpga_reload: 'False'
    freq: default_freq
    gain0: default_tx_gain
    gain1: '10'
    if_gain0: '0'
    if_gain1: '20'
    in_clk: ONBOARD
    lpf_mode: disabled
    maxoutbuf: '0'
    metadata: 'False'
    minoutbuf: '0'
    nchan: '1'
    out_clk: 'False'
    ref_clk: '0'
    sample_rate: samp_rate
    sampling: internal
    show_pmic: 'False'
    smb: '0'
    tamer: internal
    trigger0: 'False'
    trigger1: 'False'
    trigger_role0: master
    trigger_role1: master
    trigger_signal0: J51_1
    trigger_signal1: J51_1
    tsb_tag_name: '"packet_len"'
    use_dac: 'False'
    verbosity: warning
    xb200: none
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1552, 436.0]
    rotation: 0
    state: enabled
- name: bladeRF_source_0
  id: bladeRF_source
  parameters:
    affinity: ''
    alias: ''
    bias_tee0: 'False'
    bias_tee1: 'False'
    bw: '0.5'
    comment: ''
    dac: '10000'
    dc_calibration: LPF_TUNING
    dc_offset_mode0: '2'
    dc_offset_mode1: '0'
    device_id: '0'
    fpga_image: ''
    fpga_reload: 'False'
    freq: rf_freq
    gain0: rx_gain
    gain1: '10'
    gain_mode0: 'False'
    gain_mode1: 'False'
    if_gain0: '0'
    if_gain1: '20'
    in_clk: ONBOARD
    iq_balance_mode0: '2'
    iq_balance_mode1: '0'
    lpf_mode: disabled
    maxoutbuf: '0'
    metadata: 'False'
    minoutbuf: '0'
    nchan: '1'
    out_clk: 'False'
    ref_clk: '0'
    sample_rate: samp_rate
    sampling: internal
    show_pmic: 'False'
    smb: '0'
    tamer: internal
    trigger0: 'False'
    trigger1: 'False'
    trigger_role0: master
    trigger_role1: master
    trigger_signal0: J51_1
    trigger_signal1: J51_1
    use_dac: 'False'
    verbosity: warning
    xb200: none
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [96, 948.0]
    rotation: 0
    state: enabled
- name: blocks_message_debug_0
  id: blocks_message_debug
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    en_uvec: 'True'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [768, 1600.0]
    rotation: 0
    state: disabled
- name: blocks_message_debug_1
  id: blocks_message_debug
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    en_uvec: 'True'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [2080, 1136.0]
    rotation: 180
    state: true
- name: blocks_message_strobe_0
  id: blocks_message_strobe
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    msg: pmt.cons(pmt.PMT_NIL,pmt.init_u8vector(9,(71,78,85,32,82,97,100,105,111)))
    period: '50'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [168, 436.0]
    rotation: 0
    state: enabled
- name: blocks_message_strobe_0_0
  id: blocks_message_strobe
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    msg: pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(pld_data), pld_data))
    period: '500'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [72, 316.0]
    rotation: 0
    state: disabled
- name: blocks_multiply_const_vxx_0
  id: blocks_multiply_const_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    const: '0.5'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1280, 548.0]
    rotation: 0
    state: true
- name: blocks_pdu_to_tagged_stream_0
  id: blocks_pdu_to_tagged_stream
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    tag: packet_len
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 548.0]
    rotation: 0
    state: true
- name: blocks_random_pdu_0
  id: blocks_random_pdu
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length_modulo: '2'
    mask: '0xFF'
    maxoutbuf: '0'
    maxsize: '1024'
    minoutbuf: '0'
    minsize: '1024'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [144, 524.0]
    rotation: 0
    state: true
- name: blocks_repack_bits_bb_0
  id: blocks_repack_bits_bb
  parameters:
    affinity: ''
    alias: ''
    align_output: 'False'
    comment: ''
    endianness: gr.GR_MSB_FIRST
    k: '2'
    l: '1'
    len_tag_key: '""'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1456, 956.0]
    rotation: 0
    state: true
- name: blocks_repack_bits_bb_1
  id: blocks_repack_bits_bb
  parameters:
    affinity: ''
    alias: ''
    align_output: 'False'
    comment: ''
    endianness: gr.GR_MSB_FIRST
    k: '1'
    l: '8'
    len_tag_key: '"packet_len"'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1992, 956.0]
    rotation: 0
    state: true
- name: blocks_stream_to_tagged_stream_0
  id: blocks_stream_to_tagged_stream
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    len_tag_key: '"packet_len"'
    maxoutbuf: '0'
    minoutbuf: '0'
    packet_len: '2048'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1136, 708.0]
    rotation: 0
    state: disabled
- name: blocks_tagged_stream_to_pdu_0
  id: blocks_tagged_stream_to_pdu
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    tag: packet_len
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [2224, 964.0]
    rotation: 0
    state: true
- name: blocks_throttle_0
  id: blocks_throttle
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    ignoretag: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_second: samp_rate
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [840, 788.0]
    rotation: 0
    state: disabled
- name: blocks_throttle_1
  id: blocks_throttle
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    ignoretag: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_second: samp_rate
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1084.0, 768]
    rotation: 90
    state: disabled
- name: blocks_throttle_2
  id: blocks_throttle
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    ignoretag: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_second: '1'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1136, 1052.0]
    rotation: 0
    state: disabled
- name: digital_constellation_decoder_cb_0
  id: digital_constellation_decoder_cb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    constellation: bpsk
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1192, 964.0]
    rotation: 0
    state: true
- name: digital_constellation_modulator_0
  id: digital_constellation_modulator
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    constellation: bpsk
    differential: 'False'
    excess_bw: excess_bw
    log: 'False'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_symbol: '4'
    truncate: 'False'
    verbose: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1040, 524.0]
    rotation: 0
    state: enabled
- name: digital_correlate_access_code_xx_ts_0
  id: digital_correlate_access_code_xx_ts
  parameters:
    access_code: '"1110000101011010111010001001001000001110"'
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    tagname: packet_len
    threshold: thresh
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1696, 948.0]
    rotation: 0
    state: true
- name: digital_costas_loop_cc_1
  id: digital_costas_loop_cc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    order: '4'
    use_snr: 'False'
    w: phase_bw
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [888, 968.0]
    rotation: 0
    state: true
- name: digital_crc32_async_bb_0
  id: digital_crc32_async_bb
  parameters:
    affinity: ''
    alias: ''
    check: 'True'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [2160, 1140.0]
    rotation: 180
    state: disabled
- name: digital_crc32_async_bb_1
  id: digital_crc32_async_bb
  parameters:
    affinity: ''
    alias: ''
    check: 'False'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [384, 548.0]
    rotation: 0
    state: enabled
- name: digital_linear_equalizer_0
  id: digital_linear_equalizer
  parameters:
    adapt_after_training: 'True'
    affinity: ''
    alg: alg
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_taps: '64'
    sps: sps
    training_sequence: '[ ]'
    training_start_tag: corr_est
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [656, 848.0]
    rotation: 0
    state: disabled
- name: digital_linear_equalizer_1
  id: digital_linear_equalizer
  parameters:
    adapt_after_training: 'True'
    affinity: ''
    alg: alg
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_taps: '16'
    sps: '1'
    training_sequence: '[ ]'
    training_start_tag: corr_est
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [768, 1008.0]
    rotation: 0
    state: disabled
- name: digital_linear_equalizer_1_0
  id: digital_linear_equalizer
  parameters:
    adapt_after_training: 'True'
    affinity: ''
    alg: alg
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_taps: '16'
    sps: '1'
    training_sequence: '[ ]'
    training_start_tag: corr_est
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [648, 808.0]
    rotation: 0
    state: disabled
- name: digital_meas_evm_cc_0
  id: digital_meas_evm_cc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    cons: digital.constellation_bpsk()
    maxoutbuf: '0'
    meas_type: digital.evm_measurement_t.EVM_PERCENT
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [728, 1560.0]
    rotation: 0
    state: disabled
- name: digital_symbol_sync_xx_0
  id: digital_symbol_sync_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    constellation: digital.constellation_bpsk().base()
    damping: '1.0'
    loop_bw: phase_bw
    max_dev: '1.5'
    maxoutbuf: '0'
    minoutbuf: '0'
    nfilters: '128'
    osps: '1'
    pfb_mf_taps: '[]'
    resamp_type: digital.IR_MMSE_8TAP
    sps: sps
    ted_gain: '1.0'
    ted_type: digital.TED_EARLY_LATE
    type: cc
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [376, 988.0]
    rotation: 0
    state: true
- name: elen90089_packet_mac_tx_0
  id: elen90089_packet_mac_tx
  parameters:
    affinity: ''
    alias: ''
    bps: bps_payload
    comment: ''
    freq: rf_freq
    gain: tx_gain
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [368, 308.0]
    rotation: 0
    state: disabled
- name: epy_block_0
  id: epy_block
  parameters:
    _source_code: "\"\"\"\nEmbedded Python Block\n\"\"\"\n\nimport numpy as np\nfrom\
      \ gnuradio import gr\nimport pmt\n\nclass blk(gr.sync_block):\n    \"\"\"Packet\
      \ Format\"\"\"\n\n    def __init__(self):\n        gr.sync_block.__init__(self,\n\
      \            name = \"Packet Format\",\n            in_sig = None,\n       \
      \     out_sig = None)\n        self.message_port_register_in(pmt.intern('PDU_in'))\n\
      \        self.message_port_register_out(pmt.intern('PDU_out'))\n        self.set_msg_handler(pmt.intern('PDU_in'),\
      \ self.handle_msg)\n\n    def handle_msg(self, msg):\n        inMsg = pmt.to_python\
      \ (msg)\n        pld = inMsg[1]\n        # print (pld)\n        mLen = len(pld)\n\
      \        # print (mLen)\n        if (mLen > 0):\n            # char_list = [85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,225,90,232,147]\n\
      \            \n            char_list = [225,90,232,146,14]\n            char_list.append\
      \ (mLen >> 8)\n            char_list.append (mLen & 255)\n            char_list.append\
      \ (mLen >> 8)\n            char_list.append (mLen & 255)\n            char_list.extend\
      \ (pld)\n            # print (char_list)\n            out_len = len(char_list)\n\
      \            # print (out_len)\n            self.message_port_pub(pmt.intern('PDU_out'),\
      \ pmt.cons(pmt.PMT_NIL,pmt.init_u8vector(out_len,(char_list))))\n"
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    _io_cache: ('Packet Format', 'blk', [], [('PDU_in', 'message', 1)], [('PDU_out',
      'message', 1)], 'Packet Format', [])
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [600, 552.0]
    rotation: 0
    state: true
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import numpy as np
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 100.0]
    rotation: 0
    state: true
- name: packet_phy_rx_0
  id: packet_phy_rx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    excess_bw: excess_bw
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [448, 1416.0]
    rotation: 0
    state: disabled
- name: packet_phy_tx_1
  id: packet_phy_tx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    excess_bw: excess_bw
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [640, 324.0]
    rotation: 0
    state: disabled
- name: qtgui_const_sink_x_0
  id: qtgui_const_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: '"blue"'
    color10: '"red"'
    color2: '"red"'
    color3: '"red"'
    color4: '"red"'
    color5: '"red"'
    color6: '"red"'
    color7: '"red"'
    color8: '"red"'
    color9: '"red"'
    comment: ''
    grid: 'True'
    gui_hint: 4, 1, 1, 1
    label1: Header
    label10: ''
    label2: Payload
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    legend: 'True'
    marker1: '0'
    marker10: '0'
    marker2: '0'
    marker3: '0'
    marker4: '0'
    marker5: '0'
    marker6: '0'
    marker7: '0'
    marker8: '0'
    marker9: '0'
    name: '"Received Symbols"'
    nconnections: '2'
    size: (64 + 48)//1
    style1: '0'
    style10: '0'
    style2: '0'
    style3: '0'
    style4: '0'
    style5: '0'
    style6: '0'
    style7: '0'
    style8: '0'
    style9: '0'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_TAG
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '"phase_est"'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    xmax: '2'
    xmin: '-2'
    ymax: '2'
    ymin: '-2'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [728, 1476.0]
    rotation: 0
    state: disabled
- name: qtgui_const_sink_x_1
  id: qtgui_const_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: '"blue"'
    color10: '"red"'
    color2: '"red"'
    color3: '"red"'
    color4: '"red"'
    color5: '"red"'
    color6: '"red"'
    color7: '"red"'
    color8: '"red"'
    color9: '"red"'
    comment: ''
    grid: 'False'
    gui_hint: ''
    label1: ''
    label10: ''
    label2: ''
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    legend: 'True'
    marker1: '0'
    marker10: '0'
    marker2: '0'
    marker3: '0'
    marker4: '0'
    marker5: '0'
    marker6: '0'
    marker7: '0'
    marker8: '0'
    marker9: '0'
    name: '""'
    nconnections: '1'
    size: '1024'
    style1: '0'
    style10: '0'
    style2: '0'
    style3: '0'
    style4: '0'
    style5: '0'
    style6: '0'
    style7: '0'
    style8: '0'
    style9: '0'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    xmax: '2'
    xmin: '-2'
    ymax: '2'
    ymin: '-2'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1120, 892.0]
    rotation: 0
    state: true
- name: qtgui_number_sink
  id: qtgui_number_sink
  parameters:
    affinity: ''
    alias: ''
    autoscale: 'False'
    avg: '0.01'
    color1: ("black", "black")
    color10: ("black", "black")
    color2: ("black", "black")
    color3: ("black", "black")
    color4: ("black", "black")
    color5: ("black", "black")
    color6: ("black", "black")
    color7: ("black", "black")
    color8: ("black", "black")
    color9: ("black", "black")
    comment: ''
    factor1: '1'
    factor10: '1'
    factor2: '1'
    factor3: '1'
    factor4: '1'
    factor5: '1'
    factor6: '1'
    factor7: '1'
    factor8: '1'
    factor9: '1'
    graph_type: qtgui.NUM_GRAPH_HORIZ
    gui_hint: 5, 0, 1, 2
    label1: Header EVM
    label10: ''
    label2: Payload EVM
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    max: '100.0'
    min: '0'
    name: '"Recevier EVM"'
    nconnections: '1'
    type: float
    unit1: '%'
    unit10: ''
    unit2: '%'
    unit3: ''
    unit4: ''
    unit5: ''
    unit6: ''
    unit7: ''
    unit8: ''
    unit9: ''
    update_time: '0.10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [936, 1524.0]
    rotation: 0
    state: disabled
- name: qtgui_time_sink_x_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: 2, 0, 1, 2
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '"Transmit Waveform"'
    nconnections: '1'
    size: '512'
    srate: '1'
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_TAG
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '"packet_len"'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [616, 220.0]
    rotation: 180
    state: disabled
- name: qtgui_time_sink_x_0_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'True'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: 3, 0, 1, 2
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '"Received Waveform"'
    nconnections: '1'
    size: '512'
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.01'
    tr_mode: qtgui.TRIG_MODE_NORM
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [448, 1556.0]
    rotation: 0
    state: disabled
- name: qtgui_time_sink_x_1
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'True'
    gui_hint: 4, 0, 1, 1
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '0'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: '32'
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.5'
    tr_mode: qtgui.TRIG_MODE_NORM
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '"corr_start"'
    type: float
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1.2'
    ymin: '0'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [728, 1396.0]
    rotation: 0
    state: disabled
- name: qtgui_time_sink_x_2
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: '1024'
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1560, 708.0]
    rotation: 0
    state: disabled
- name: qtgui_time_sink_x_2_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'True'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '"Receiver"'
    nconnections: '1'
    size: '1024'
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.01'
    tr_mode: qtgui.TRIG_MODE_NORM
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [376, 884.0]
    rotation: 0
    state: true
- name: qtgui_time_sink_x_3
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '"Transmitter"'
    nconnections: '1'
    size: '1024'
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1552, 684.0]
    rotation: 0
    state: true

connections:
- [analog_sig_source_x_0, '0', blocks_stream_to_tagged_stream_0, '0']
- [bladeRF_source_0, '0', digital_symbol_sync_xx_0, '0']
- [bladeRF_source_0, '0', qtgui_time_sink_x_0_0, '0']
- [bladeRF_source_0, '0', qtgui_time_sink_x_2_0, '0']
- [blocks_message_strobe_0, strobe, blocks_random_pdu_0, generate]
- [blocks_message_strobe_0_0, strobe, elen90089_packet_mac_tx_0, mac_sdu]
- [blocks_multiply_const_vxx_0, '0', bladeRF_sink_0, '0']
- [blocks_multiply_const_vxx_0, '0', qtgui_time_sink_x_2, '0']
- [blocks_multiply_const_vxx_0, '0', qtgui_time_sink_x_3, '0']
- [blocks_pdu_to_tagged_stream_0, '0', digital_constellation_modulator_0, '0']
- [blocks_random_pdu_0, pdus, digital_crc32_async_bb_1, in]
- [blocks_repack_bits_bb_0, '0', digital_correlate_access_code_xx_ts_0, '0']
- [blocks_repack_bits_bb_1, '0', blocks_tagged_stream_to_pdu_0, '0']
- [blocks_tagged_stream_to_pdu_0, pdus, blocks_message_debug_1, print]
- [blocks_tagged_stream_to_pdu_0, pdus, digital_crc32_async_bb_0, in]
- [blocks_throttle_2, '0', digital_constellation_decoder_cb_0, '0']
- [digital_constellation_decoder_cb_0, '0', blocks_repack_bits_bb_0, '0']
- [digital_constellation_modulator_0, '0', blocks_multiply_const_vxx_0, '0']
- [digital_correlate_access_code_xx_ts_0, '0', blocks_repack_bits_bb_1, '0']
- [digital_costas_loop_cc_1, '0', blocks_throttle_2, '0']
- [digital_costas_loop_cc_1, '0', digital_constellation_decoder_cb_0, '0']
- [digital_costas_loop_cc_1, '0', qtgui_const_sink_x_1, '0']
- [digital_crc32_async_bb_1, out, epy_block_0, PDU_in]
- [digital_linear_equalizer_0, '0', digital_costas_loop_cc_1, '0']
- [digital_linear_equalizer_1, '0', digital_costas_loop_cc_1, '0']
- [digital_linear_equalizer_1_0, '0', digital_costas_loop_cc_1, '0']
- [digital_meas_evm_cc_0, '0', qtgui_number_sink, '0']
- [digital_symbol_sync_xx_0, '0', digital_costas_loop_cc_1, '0']
- [digital_symbol_sync_xx_0, '0', digital_linear_equalizer_0, '0']
- [digital_symbol_sync_xx_0, '0', digital_linear_equalizer_1, '0']
- [digital_symbol_sync_xx_0, '0', digital_linear_equalizer_1_0, '0']
- [elen90089_packet_mac_tx_0, mac_pdu, packet_phy_tx_1, phy_sdu]
- [epy_block_0, PDU_out, blocks_pdu_to_tagged_stream_0, pdus]
- [packet_phy_rx_0, '0', qtgui_time_sink_x_1, '0']
- [packet_phy_rx_0, '1', digital_meas_evm_cc_0, '0']
- [packet_phy_rx_0, '1', qtgui_const_sink_x_0, '0']
- [packet_phy_rx_0, '2', qtgui_const_sink_x_0, '1']
- [packet_phy_rx_0, phy_sdu, blocks_message_debug_0, print_pdu]
- [packet_phy_tx_1, '0', bladeRF_sink_0, '0']
- [packet_phy_tx_1, '0', qtgui_time_sink_x_0, '0']

metadata:
  file_format: 1
